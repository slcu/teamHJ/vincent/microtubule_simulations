#include "Parametres.hpp"
#include <string>
#include <iostream>
#include <map>
#include <fstream>

#define verbose 0
#define deb(x) if (verbose==1){cout << x << endl;}
#define deb2(x) if (verbose==2){cout << x << endl;}

using namespace std;

Parametres::Parametres(string config)
{
    m_proprietesD["Kgr_sh_Plus"]=0.0;
    m_proprietesD["Ksh_gr_Plus"]=0.0;
    m_proprietesD["V_gr_Plus"]=0.0;
    m_proprietesD["V_sh_Plus"]=0.0;
    m_proprietesD["V_sh_Moins"]=0.0;
    m_proprietesD["Angle_bundle"]=0.0;
    m_proprietesD["K_nobundle_shrink"]=0.0;
    m_proprietesD["K_nobundle_cross"]=0.0;
    m_proprietesD["K_nobundle_catastrophe"]=0.0;
    m_proprietesD["D_bundle"]=0.0;
    m_proprietesD["Angle_mb_limite"]=0.0;
    m_proprietesD["Angle_mb_trajectoire"]=0.0;
    m_proprietesD["Angle_cut"]=0.0;//angle defining the category that is targeted by katanin
    m_proprietesD["tan_Angle_mb"]=0.0;
    m_proprietesD["epaisseur_corticale"]=0.0;
    m_proprietesD["poids_previous_vect"]=0.0;
    m_proprietesD["d_mb"]=0.0;
    m_proprietesD["part_alea_alea"]=0.0;
    m_proprietesD["part_alea_fixe"]=0.0;
    m_proprietesD["proba_detachement_par_step_par_microtubule"]=0.0;
    m_proprietesD["proba_initialisation_par_step"]=0.0;
    m_proprietesD["proba_tocut"]=0.0;
    m_proprietesD["part_influence_influence"]=0.0;
    m_proprietesD["part_influence_normale"]=0.0;
    m_proprietesD["taille_microtubule"]=0.;
    
    m_proprietesI["gr_st_plus"]=0;
    m_proprietesI["gr_st_moins"]=0;
    m_proprietesI["nb_microtubules_init"]=0;
    m_proprietesI["nb_max_steps"]=0;
    m_proprietesI["garbage_steps"]=0;
    m_proprietesI["vtk_steps"]=0;
    m_proprietesI["stop"]=0;
    m_proprietesI["details"]=0;
    
    m_proprietesS["nom_output_vtk"]="";
    m_proprietesS["nom_input_vtk"]="";
    m_proprietesS["nom_rapport"]="";
    m_proprietesS["nom_input_vtk_ref"]="";
    m_proprietesS["nom_folder_input_vtk"]="";
    m_proprietesS["nom_folder_output_vtk"]="";
    m_proprietesS["nom_config"]="";
    
    m_proprietesI["decision_rencontre"]=0;
    m_proprietesI["decision_accrochage"]=0;
    m_proprietesI["decision_cut"]=0;
    m_proprietesI["cortical"]=0;
    
    m_proprietesI["save_events"]=100;
    
    
    bool b = Load(config);
    if (!b) {exit(0);}
}



void Parametres::setProprieteI(string p, int k)					
{
    m_proprietesI[p]=k;
}
void Parametres::setProprieteD(string p, double k)					
{
    m_proprietesD[p]=k;
}
void Parametres::setProprieteS(string p,string k)
{
    m_proprietesS[p]=k;
}

 
int Parametres::getProprieteI(string p)							
{
    if (m_proprietesI.count(p)>0){return m_proprietesI[p];}
    else {
        cout << "erreur de getPropriete I "<<p<<endl;
        exit(0);
    }
    
}
double Parametres::getProprieteD(string p)
{
    if (m_proprietesD.count(p)>0){return m_proprietesD[p];}
    else {
        cout << "erreur de getPropriete D "<<p<<endl;
        exit(0);
    }
}
string Parametres::getProprieteS(string p)
{
    if (m_proprietesS.count(p)>0){return m_proprietesS[p];}
    else {
        cout << "erreur de getPropriete S "<<p<<endl;
        exit(0);
    }
}

void Parametres::initialise_default()
{
    //Kawamura et al 2008
    //allard et al 2010 Mechanisms of Self-Organization of Cortical Microtubules in Plants Revealed by Computational Simulations
    //Hawkins et al 2010
    //Murata et al 2005
    //Dixit et al 2004 Encounters between Dynamic Cortical Microtubules Promote Ordering of the Cortical Array through Angle-Dependent Modifications of Microtubule Behavior
    //Chan et al 2003
    
    m_proprietesD["Kgr_sh_Plus"]=0.01;// /s                              allard et al 2010
    m_proprietesD["Ksh_gr_Plus"]=0.04;// /s                              allard et al 2010
    m_proprietesD["V_gr_Plus"]=0.08;// um/s
    m_proprietesD["V_sh_Plus"]=0.16;// um/s                              Kawamura et al 2008
    m_proprietesD["V_sh_Moins"]=0.09;// um/s                             Kawamura et al 2008
    m_proprietesD["Angle_bundle"]=0.69813170079773179;//                                  Dixit et al 2004
    m_proprietesD["K_nobundle_shrink"]=0.4;// um/s
    m_proprietesD["K_nobundle_cross"]=1-m_proprietesD["K_nobundle_shrink"];// um/s
    m_proprietesD["K_nobundle_catastrophe"]=0.4;//                       Dixit et al 2004
    m_proprietesD["D_bundle"]=49;// nm                                 Chan et al 2003
    m_proprietesD["Angle_mb"]=0.17453292519943295;//( = 10 deg)          personnal estimate
    //m_proprietesD["Angle_mb"]=0.5;//( = 40 deg)          personnal estimate
    m_proprietesD["Angle_cut"]=0.6;
    m_proprietesD["tan_Angle_mb"]=0.83909963117727993;//( = 40 deg)          personnal estimate
    //m_proprietesD["poids_previous_vect"]=0.83909963117727993;//( = 40 deg) concerns the relative weight between the previous element and environment
    m_proprietesD["poids_previous_vect"]=0.5;//( = 40 deg) concerns the relative weight between the previous element and environment
    m_proprietesD["d_mb"]=10;//( = distance to the membrane)          personnal estimate    
    
    m_proprietesI["gr_st_plus"]=1; //state of growth of the plus side
    m_proprietesI["gr_st_moins"]=0; //state of growth of the moins side
    
    m_proprietesD["part_alea_alea"]=1;//dans Microtubule, la ponderation de la part aleatoire
    m_proprietesD["part_alea_fixe"]=40;//dans Microtubule, la ponderation de la part fixe
    
    m_proprietesI["nb_microtubules_init"]=100;
    
    m_proprietesI["nb_max_steps"]=1000;
    
    m_proprietesI["garbage_steps"]=500;
    m_proprietesI["vtk_steps"]=500;
    m_proprietesI["stop"]=0;
    m_proprietesI["details"]=0;

    m_proprietesD["part_influence_influence"]=0;//dans Microtubule, la ponderation de la part liee aux forces
    m_proprietesD["part_influence_normale"]=1;//dans Microtubule, la ponderation de la part liees au mouvement normal
    
    m_proprietesS["nom_output_vtk"]="sortie_standart.vtk";
    m_proprietesS["nom_input_vtk"]="ellipsoid_3_final_convert.vtk";
    m_proprietesS["nom_input_vtk_ref"]="contourPave.vtk";
    m_proprietesS["nom_rapport"]="rapport.txt";
    m_proprietesS["nom_folder_output_vtk"]="./";
    m_proprietesS["nom_folder_input_vtk"]="./";
    m_proprietesS["nom_config"]="config.ini";
    
    m_proprietesD["proba_detachement_par_step_par_microtubule"]=0.001;
    m_proprietesD["proba_initialisation_par_step"]=0.1;
    m_proprietesD["proba_tocut"]=0.01;

    
    m_proprietesD["taille_microtubule"]=8.;//nm
    
    //a rajouter
    //"vitesse_relative_plus_moins"
    //"proba_shrink_si_rencontre"
    //"proba_stop_si_rencontre"
    //"proba_scission"
    //"taille_microtubule"
    m_proprietesI["decision_rencontre"]=0;
    m_proprietesI["decision_accrochage"]=0;
    m_proprietesI["decision_cut"]=0;//se réfère à si oui ou non on coupe les microtubules, c'est dans to_cut que ce point est testé
    m_proprietesI["cortical"]=0;
    
    m_proprietesI["save_events"]=100;
    
    
}





bool Parametres::Load(string file)
{
    ifstream inFile(file.c_str());

    if (!inFile.good())
    {
        cout << "Cannot read configuration file " << file << endl;
        return false;
    }

    while (inFile.good() && ! inFile.eof())
    {
        string line;
        getline(inFile, line);

        // filter out comments
        if (!line.empty())
        {
            size_t pos = line.find('#');

            if (pos != string::npos)
            {
                line = line.substr(0, pos);
            }
        }

        // split line into key and value
        if (!line.empty())
        {
            size_t pos = line.find('=');

            if (pos != string::npos)
            {
                string key     = Trim(line.substr(0, pos));
                string value   = Trim(line.substr(pos + 1));

                if (!key.empty() && !value.empty())
                {
                    remplir(key, value);
                }
            }
        }
    }

    return true;
}

void Parametres::remplir(string key, string value)
{
    if (m_proprietesD.count(key)>0)
    {m_proprietesD[key]=atof(value.c_str());
    //cout << "type : "<<" double "<<" key : "<<key<<" value : "<<value <<endl;
    }
    else if (m_proprietesI.count(key)>0)
    {m_proprietesI[key]=atoi(value.c_str());
    //cout << "type : "<<" integer "<<" key : "<<key<<" value : "<<value <<endl;
    }
    else if (m_proprietesS.count(key)>0)
    {m_proprietesS[key]=value.c_str();
    //cout << "type : "<<" string "<<" key : "<<key<<" value : "<<value <<endl;
    }
    else
    {cout << "souci parametre inconnu" <<endl;
    exit(0);
    }
}

string Parametres::Trim(const string& str)
{
    size_t first = str.find_first_not_of(" \t");

    if (first != string::npos)
    {
        int last = str.find_last_not_of(" \t");

        return str.substr(first, last - first + 1);
    }
    else
    {
        return "";
    }
}

