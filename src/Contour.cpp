//classe Contour

#include "Contour.hpp"
#include <iostream>

//#include "vtkPolyDataReader.h"
//#include "vtkTransformPolyDataFilter.h"
//#include "vtkTransform.h"
//#include "vtkPolyData.h"
//#include "vtkSmartPointer.h"
//#include "vtkPolyDataWriter.h"
#include "vtkDoubleArray.h"
#include "vtkFloatArray.h"
#include "vtkCellData.h"
#include "vtkPointData.h"
#include "vtkMassProperties.h"
#include "vtkPolyDataNormals.h"

#include <map>
#include <string>
#include <vector>
#include <sstream>
#include "ElementPool.hpp"
#include "Element.hpp"
#include "Anchor.hpp"
#include "Shape.hpp"
#include "utility.hpp"


#define verbose 0
#define deb(x) if (verbose==1){cout << x << endl;}

using namespace std;

std::ostream& Contour::Print( std::ostream &os) const
{
    os << "Contour "<<this->getId()<<"/"<<this->getType()<<"/"<<this<<endl;
    
return os;
}



ostream& operator<<(ostream& os, Contour& m)
{
    m.Print(os);
    return os;
}

Contour::Contour(Parametres *params, ElementPool *ep, Id r):Structure()
{
        m_id=r;
        m_type=2;
        m_elementPool=ep;
        m_params=params;
        ep->setContour(this);
}




void Contour::open()
{
    string s_filename = m_params->getProprieteS("nom_input_vtk");
    string s_ref = m_params->getProprieteS("nom_input_vtk_ref");
    string r_ref = m_params->getProprieteS("nom_folder_input_vtk");
    string complete_name=r_ref+s_filename;
    string complete_ref_name=r_ref+s_ref;
    char *filename = (char*)complete_name.c_str();
	vtkSmartPointer<vtkPolyDataReader> contour = vtkSmartPointer<vtkPolyDataReader>::New();	
	contour->SetFileName(filename);
	contour->Update();
    
    char *refname = (char*)complete_ref_name.c_str();
	vtkSmartPointer<vtkPolyDataReader> contour_ref = vtkSmartPointer<vtkPolyDataReader>::New();	
	contour_ref->SetFileName(refname);
	contour_ref->Update();
	
    vtkSmartPointer<vtkMassProperties> properties =	vtkSmartPointer<vtkMassProperties>::New();
//     properties->SetInput(contour->GetOutput());
    properties->SetInputConnection(contour->GetOutputPort());
    cout<<"surface "<<properties->GetSurfaceArea()<<endl;

    vtkSmartPointer<vtkMassProperties> properties_ref =	vtkSmartPointer<vtkMassProperties>::New();
//     properties_ref->SetInput(contour_ref->GetOutput());
    properties_ref->SetInputConnection(contour_ref->GetOutputPort());
    cout<<"surface_ref "<<properties_ref->GetSurfaceArea()<<endl;
	
    double result=m_params->getProprieteD("proba_initialisation_par_step")*properties->GetSurfaceArea()/properties_ref->GetSurfaceArea();
    m_params->setProprieteD("proba_initialisation_par_step",result);
    
	vtkSmartPointer<vtkTransform> translation =	vtkSmartPointer<vtkTransform>::New();
	//translation->RotateX(90);
	//translation->Translate(600.0, 600.0, 600.0);
	//translation->Scale(1200.0, 1200.0, 1200.0);
	//translation->Scale(1./4., 1./4., 1./4.);
	
	
	vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
// 	transformFilter->SetInput(contour->GetOutput());
    transformFilter->SetInputConnection(contour->GetOutputPort());
	transformFilter->SetTransform(translation);
	transformFilter->Update();
	
	//ici j'emploierai des anchors et des Shape
	vtkSmartPointer<vtkPolyData> contourData = transformFilter->GetOutput();
	vtkIdType numPoints = contourData->GetNumberOfPoints();
	//cout << "nb of points : "<<numPoints<<endl;
    vtkSmartPointer<vtkPolyDataNormals> normalGenerator = vtkSmartPointer<vtkPolyDataNormals>::New();
    normalGenerator->SetInputData(contourData);
    normalGenerator->ComputePointNormalsOn();
    normalGenerator->ComputeCellNormalsOff();
    normalGenerator->Update();

	// vtkFloatArray* normalDataFloat = vtkFloatArray::SafeDownCast(normalGenerator->GetOutput()->GetPointData()->GetArray("Normals"));
    vtkFloatArray* normalDataFloat = vtkFloatArray::SafeDownCast(contour->GetOutput()->GetPointData()->GetArray("Normals"));

	//vtkDoubleArray* normalDataDouble = vtkDoubleArray::SafeDownCast(polydata->GetPointData()->GetArray("Normals"));
	//if(normalDataFloat)
    //{
    int nc = normalDataFloat->GetNumberOfTuples();
    std::cout << "There are " << nc
            << " components in normalDataFloat" << std::endl;
    // return true;
    
    if (!normalDataFloat)
    {
		cout << "il ne semble pas y avoir de normales"<<endl;
	}
    for (int i = 0; i<numPoints; i++)
    {
		//cout << "point " << i << " : " << *(contourData->GetPoint(i))<< " / " << *(contourData->GetPoint(i)+1)<< " / " << *(contourData->GetPoint(i)+2) <<endl;

		Element* current = m_elementPool->giveElement();
        current->setId_vtk(i);
		vector<double> pos;
		double* posvtk = contourData->GetPoint(i);
		pos.push_back(*posvtk);
		pos.push_back(*(posvtk+1));
		pos.push_back(*(posvtk+2));

        for(auto const& component: pos) {
            if(component < 0){
                std::cerr << "At least one of your vertices has a negative component" << std::endl <<
                "expect a segfault!" <<std::endl;
            }
        }

        vector<double> dir;
		double* dirvtk = normalDataFloat->GetTuple(i);
		dir.push_back(*dirvtk);
		dir.push_back(*(dirvtk+1));
		dir.push_back(*(dirvtk+2));
        dir=normalise(dir);
		//deb("tata 1");
		Anchor a;
		Shape s;
		a.setPosition(pos);
		s.setDirection(dir);
        if (current)
        {
                int nb = contour->GetOutput()->GetPointData()->GetNumberOfArrays();
                for (int n = 0; n<nb;n++)
                {
                    //cout << "array : " << n << " nom : " << contour->GetOutput()->GetPointData()->GetArrayName(n) << endl;
                    string nom = contour->GetOutput()->GetPointData()->GetArrayName(n);
                    char *array = (char*)contour->GetOutput()->GetPointData()->GetArrayName(n);
                    int nbc=contour->GetOutput()->GetPointData()->GetArray(array)->GetNumberOfComponents();
                    //cout << "array : " << n << " nb : " << nbc << endl;
                    if (nbc == 1)
                    {
                        double *d = contour->GetOutput()->GetPointData()->GetScalars(array)->GetTuple(i);
                        //cout << "element : "<< *d<<endl;
                        current->setPropriete(nom, *d);
                    }
                    else if (nbc==3)
                    {
                        double *d = contour->GetOutput()->GetPointData()->GetVectors(array)->GetTuple(i);
                        //cout << "elements : "<< *d << " " << *(d+1) << " " << *(d+2) <<endl;
                        vector<double> v;
                        v.push_back(*d);
                        v.push_back(*(d+1));
                        v.push_back(*(d+2));
                        current->setPropriete(nom, v);
                    }
                    else
                    {
                        //cout << "number of components not understandable (by me) in "<< nom <<endl;
                        deb("number of components not understandable (by me)");
                    }
                }
                
                current->setAnchor(a);
                current->setShape(s);
                current->setStructure(this);
                //deb("tata 2");
                m_elementPool->spaceContourRegister(current);
                m_body.push_back(current);
        }
	}
    deb("fin de Contour::open");
}

vector<double> Contour::getElementRandom()
{
    int taille = m_body.size();
    int choix = (int) ((float)rand() * taille / (float)RAND_MAX);
    //cout << "choix "<<choix<<" / "<<taille <<endl;
    Element *e = m_body.at(choix);
    //cout << *e<<endl;
    vector<double> pos;
    Anchor *a=e->getAnchor();
    pos.push_back(*a->getX() - 10 * e->getShape().getDirection()[0]);
    pos.push_back(*a->getY() - 10 * e->getShape().getDirection()[1]);
    pos.push_back(*a->getZ() - 10 * e->getShape().getDirection()[2]);
    
    vector<double> dir;
    dir.push_back(-1+2*(float)rand()/(float)RAND_MAX);
    dir.push_back(-1+2*(float)rand()/(float)RAND_MAX);
    dir.push_back(-1+2*(float)rand()/(float)RAND_MAX);
    dir=normalise(dir);
    //ici on s'assure que la direction initiale est tangente à la mb
    double scalaire =  dir[0]*e->getShape().getDirection()[0] + dir[1]*e->getShape().getDirection()[1] + dir[2]*e->getShape().getDirection()[2];
    dir[0]=dir[0] - scalaire * e->getShape().getDirection()[0];
    dir[1]=dir[1] - scalaire * e->getShape().getDirection()[1];
    dir[2]=dir[2] - scalaire * e->getShape().getDirection()[2];
    dir=normalise(dir);
    pos.push_back(dir[0]);
    pos.push_back(dir[1]);
    pos.push_back(dir[2]);
    return pos;
}




