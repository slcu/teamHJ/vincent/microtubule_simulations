//shape of an element
#include <vector>
#include "Shape.hpp"

using namespace std;

Shape::Shape(){}
Shape::Shape(double x, double y, double z)
{
    std::vector<double>direction;
    direction.push_back(x);
    direction.push_back(y);
    direction.push_back(z);
    m_direction=direction;
}

std::vector<double> Shape::getDirection() const
{
    return m_direction;
}
        
void Shape::setDirection(std::vector<double> direction)
{
    m_direction=direction;    
}        




