//classe Element
//#include <random> 
#include <cstdlib>
#include "Element.hpp"
#include "ElementPool.hpp"
#include "Structure.hpp"
#include "Contour.hpp"
#include "math.h"
using namespace std;

#define verbose 0
#define deb(x) if (verbose==1){cout << x << endl;}

ostream& operator<<(ostream& os, Element p)
{
    cout.precision(4);
    os << "Element "<<p.getId();
    os << "->";
    os << *p.getAnchor()->getX();
    os << "/";
    os << *p.getAnchor()->getY();
    os << "/";
    os << *p.getAnchor()->getZ();
    os << "\t";
    os << p.getShape().getDirection()[0];
    os << "/";
    os << p.getShape().getDirection()[1];
    os << "/";
    os <<   p.getShape().getDirection()[2];
    os << "\t";
    os << p.isAlive();
    os << "\t";
    os << p.getAge();
    os << "\t";
    os << p.getStructure();
    return os;
}

/*
 *      Constructeurs 
 * 
 *      IMPORTANT : 
 * dans l'etat actuel des choses, il FAUT un Structure cree en amont
 * dans l'etat actuel des choses, il FAUT un Structure cree en amont
 * 
 */

Element::Element()
{
    if (verbose==2) {cout<<"empty constructor of Element not to be used!"<<endl;}
}


Element::Element(Parametres *params, ElementPool* ep, Id id)
{
    m_id=id;
    m_id_vtk=-1;
    m_ep=ep;
    
    m_age=0;
    m_alive=1;
    
    m_structure=NULL;
    m_structure_type=-1;
    
    m_previous=NULL;
    
    m_proprietes["contact"]=0;
    
    m_proprietes["nearest_contour"]=0;
    m_proprietes["nearest_contour_d"]=0.;
    m_proprietes["D_pos_dir2lim_mbFn"]=-10.;
    m_proprietes["to_cut"]=0.;
    m_proprietes["nearest_contour_vtk"]=-1;
    m_proprietes["cortical"]=0;
}


// destructeur
Element::~Element(){
    //PROBLEME DE DEFINITION DE M_ID
	//if (verbose == 1){ cout << endl<<"Element de rang "<<m_id<<" detruit"<<endl;}
}

void Element::setDead()
{
    m_alive=0;
}

int Element::isAlive()
{
    if (m_alive)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}




void Element::setStructure(Structure* id)
{
    deb("setStructure(Structure* id)");
    m_structure=id;
    if (id == NULL) 
    {
        m_structure_type=-1;
        m_structure_id=-1;
    }
    else
    {
        m_structure_type=id->getType();
        m_structure_id=id->getId();
    }
}

Structure* Element::getStructure()
{
    return m_structure;
}
int Element::getStructureType() const
{
    return m_structure_type;
}
int Element::getStructureId() const
{
    return m_structure_id;
}

Element* Element::getPrevious()
    {return m_previous;}
    
void Element::setPrevious(Element* previous)
    {m_previous=previous;}


int Element::isMember() const
{
    //defines if the element is in a structure
    if ( m_structure != NULL ){return 1;}
    else {return 0;}
}

/*
 * deux comportements de base :
 * - decay : ne disparait pas mais un parametre decroit (non utilise pour le moment)
 * - increase : accroit un parametre (non utilise pour le moment)
 */
    void Element::decay(){}
    void Element::increase(){}  

// communication variables
Shape Element::getShape() const                                	{return m_shape;}
void Element::setShape(Shape shape)                                {m_shape=shape;}


Anchor* Element::getAnchor()                               	{return &m_anchor;}
void Element::setAnchor(Anchor anchor)                             
{
    m_anchor=anchor;
}


Id Element::getId() const 					    				{return m_id;}
Id Element::getId_vtk() const 					    				{return m_id_vtk;}

void Element::setPropriete(string p, double k)					{m_proprietes[p]=k;}
void Element::setPropriete(string p, vector<double> k)			{m_proprietes_v[p]=k;}
double Element::getPropriete(string p)							{return m_proprietes[p];}
vector<double> Element::getPropriete_v(string p)					{return m_proprietes_v[p];}

    
Id Element::getAge() const
{
    return m_age;
    //return (Id)m_proprietes["age"];
}
void Element::setAge(Id age)
{
    m_age=age;
    m_proprietes["age"]=(int)age;
}



void Element::setId_vtk(Id id)
{
    m_id_vtk=id;
}




