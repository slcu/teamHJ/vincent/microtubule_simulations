include_directories(${PROJECT_SOURCE_DIR}/src/)
SET(microtubules_SRCS Parametres.cpp utility.cpp Contour.cpp ElementPool.cpp MicrotubulePool.cpp Microtubule.cpp Structure.cpp Element.cpp Anchor.cpp Shape.cpp drawScene.cpp main.cpp)
 
add_executable(programme ${microtubules_SRCS})
set_target_properties(programme PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}")
target_link_libraries(programme ${VTK_LIBRARIES})   

install(TARGETS programme DESTINATION ${PROJECT_SOURCE_DIR}/bin)
