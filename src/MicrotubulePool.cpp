//elementPool
#include "MicrotubulePool.hpp"
#include "Microtubule.hpp"
#include "ElementPool.hpp"
#include <vector>
#include <deque>
#include <list>
#include <utility>
#include "utility.hpp"
#include <fstream>
#define verbose 0
#define deb(x) if (verbose>=1){cout << x << endl;}
#define deb2(x) if (verbose>=2){cout << x << endl;}
#define deb3(x) if (verbose>=3){cout << x << endl;}
#define deb4(x) if (verbose>=4){cout << x << endl;}


using namespace std;


    std::ostream& operator<<(std::ostream& os, MicrotubulePool& p)
    {
        cout << p.maxId()<<endl;
        for (unordered_map<Id, Microtubule>::iterator it = p.getMicrotubules()->begin();it != p.getMicrotubules()->end();++it)
        {
            os << "id "<< it->first <<" microtubule "<<it->second<<endl;
        }
        return os;
    }
 
     std::ofstream& operator<<(std::ofstream& os, MicrotubulePool& p)
    {
        cout << p.maxId()<<endl;
        for (unordered_map<Id, Microtubule>::iterator it = p.getMicrotubules()->begin();it != p.getMicrotubules()->end();++it)
        {
            os << "id "<< it->first <<" microtubule "<<it->second<<endl;
        }
        return os;
    }
    
    MicrotubulePool::MicrotubulePool()
    {
        if (verbose>=1) {cout<<"constructeur de MicrotubulePool version () :"<<endl;}
        m_maxId=1;
    }
    
    MicrotubulePool::MicrotubulePool(Parametres *params, int nb_gamma_tub, ElementPool *ep)
    {
        if (verbose>=1) {cout<<"constructeur de MicrotubulePool version (int nb_gamma_tub, ElementPool *ep):"<<endl;}
//         int m_influence = 0;
        m_maxId=1;
        m_nb_gamma_tub=nb_gamma_tub;
        m_params = params;
        
        destruction_microtubule = 0;
        creation_microtubule = 0;
        cut_microtubule = 0;
        detachements=0;
        
        for (int i = 0; i < nb_gamma_tub;i++)
        {
            //vector<double> v = pointOnSphere(150, 3000,3000,3000);
            vector<double> v = pointOnContour(ep->getContour());
            Id current=maxId()+1;
            m_maxId++;
            Microtubule e(m_params, ep,this, current, v);
            m_microtubules[current]=e;
            e.setId(current);
            pair<Id,Microtubule*> p;
            p.first=current;
            p.second=&(m_microtubules[current]);
            m_gamma_tub.push_back(p);
        }
    }
    
    //MicrotubulePool::MicrotubulePool(Parametres *params, int nb_gamma_tub, double proba_detachement, double proba_initiation, ElementPool *ep)
    //{
        //if (verbose>=1) {cout<<"constructeur de MicrotubulePool version (int nb_gamma_tub, double proba_detachement, double proba_initiation, ElementPool *ep):"<<endl;}
        //int m_influence = 0;
        //m_maxId=1;
        //m_nb_gamma_tub=nb_gamma_tub;
        //m_proba_detachement = proba_detachement;
        //m_proba_initiation = proba_initiation;
        //m_ep=ep;
        //m_params = params;
        //
    //};
    
    
    MicrotubulePool::MicrotubulePool(Parametres *params, ElementPool *ep)
    {
        m_influence = params->getProprieteD("part_influence_normale");
        if (verbose>=1) {cout<<"constructeur de MicrotubulePool version (int nb_gamma_tub, double proba_detachement, double proba_initiation, ElementPool *ep):"<<endl;}
        m_maxId=1;
        m_nb_gamma_tub=params->getProprieteI("nb_microtubules_init");
        m_proba_detachement = params->getProprieteD("proba_detachement_par_step_par_microtubule");
        //m_proba_initiation = params->getProprieteD("proba_initialisation_par_step");
        m_proba_tocut = params->getProprieteD("proba_tocut");
        m_ep=ep;
        m_params = params;
        
        destruction_microtubule = 0;
        creation_microtubule = 0;
        cut_microtubule = 0;
        detachements=0;
        
    }
    
    MicrotubulePool::~MicrotubulePool()
    {       
           if (verbose>=1) {cout<<"destructeur de MicrotubulePool :"<<endl;}
    }
    
    void MicrotubulePool::initiate()
    {
        for (int i = 0; i < m_nb_gamma_tub;i++)
        {
            deb4("boucle ngamma 1")
            //vector<double> v = pointOnSphere(150, 3000,3000,3000);
            vector<double> v = pointOnContour(m_ep->getContour());
            Id current=maxId()+1;
            m_maxId++;
            deb4("boucle ngamma 2")
            Microtubule e(m_params, m_ep,this, current, v);
            m_microtubules[current]=e;
            e.setId(current);
            deb4("boucle ngamma 3")
            pair<Id,Microtubule*> p;
            p.first=current;
            p.second=&(m_microtubules[current]);
            deb4("boucle ngamma 4")
            m_gamma_tub.push_back(p);
            e.getPlus()->setStructure(&m_microtubules[current]);
        }
    }
    
    
    Id MicrotubulePool::giveMicrotubule(ElementPool *ep)
    {
        
        Id current=maxId()+1;
        m_maxId++;
        Microtubule e(m_params, ep,this, current);
        m_microtubules[current]=e;
        e.setId(current);
        e.getPlus()->setStructure(&m_microtubules[current]);
        //cout<<*e.getPlus()<<endl;
        return current;
    }

//
    Id MicrotubulePool::giveMicrotubule(ElementPool *ep, deque<Element *> body)
    {
        Id current=maxId()+1;
        m_maxId++;
        if (verbose==2) {cout<<"giveMicrotubule :"<<endl;}
        Microtubule e(m_params,ep,this, current, body);
        m_microtubules[current]=e;
        e.setId(current);
        e.getPlus()->setStructure(&m_microtubules[current]);
        //cout<<*e.getPlus()<<endl;
        return current;
    }
    
    
     Id MicrotubulePool::giveMicrotubule(ElementPool *ep, vector<double> vec)
    {
        Id current=maxId()+1;
        m_maxId++;
        if (verbose==2) {cout<<"giveMicrotubule :"<<endl;}
        Microtubule e(m_params,ep,this, current, vec);
        m_microtubules[current]=e;
        e.setId(current);
        e.getPlus()->setStructure(&m_microtubules[current]);
        //cout<<*e.getPlus()<<endl;
        return current;
    }
    
       
    
        
    
    Microtubule* MicrotubulePool::getMicrotubule(Id id)
    {
        //ici ajouter des tests de coherence
        return &m_microtubules[id];
    }
    
    unordered_map<Id, Microtubule>* MicrotubulePool::getMicrotubules()
    {
        //ici ajouter des tests de coherence
        return &m_microtubules;
    }
    
    list<Id> MicrotubulePool::getListMicrotubules()
    {
        list<Id> lm;
        for (unordered_map<Id,Microtubule>::iterator it=m_microtubules.begin();it!=m_microtubules.end();it++)
        {
            lm.push_back(it->first);
        }
        return lm;
    }
    
    Id MicrotubulePool::maxId()
    {
        return m_maxId;
    }
    
    int MicrotubulePool::getInfluence()
    {
        return m_influence;
    }
    
    
    
    void MicrotubulePool::erase(Id id)
    {
		deb("MicrotubulePool::erase(Id id)");
		destruction_microtubule+=1;
		//cout << "erase microtubule : "<< m_microtubules[id]  << endl;
		m_microtubules.erase(id);		
		deb("MicrotubulePool::erase(Id id) OVER");
	}

    int MicrotubulePool::write(string s_filename)
    {
        ofstream myfile;
        char *filename = (char*)s_filename.c_str();
        myfile.open(filename);
        myfile << *this;
        myfile.close();
        return 0;
    }

    void MicrotubulePool::evolve(ElementPool *ep)
    {
        //version avec trois probas : initiation, separation et coupure
        deb("MicrotubulePool::evolve(ElementPool *ep)");        
        //srand(time(0)+getpid());
        //coupure
        list<Element *> l_tocut = m_ep->getlist_tocut();        
        int k = 0;
        for (list<Element *>::iterator it=l_tocut.begin();it!=l_tocut.end();it++)
        {   
            //deb3("test...");
            double proba = (float)rand()/(float)RAND_MAX;
            if ((proba < m_proba_tocut))
            {
                deb3("ici...");
                //cout << **it <<  endl;
                //cout << "k = "<< k << endl;
                Structure *s=(*it)->getStructure();
                deb3("... ici"); 
                s->scission(*it);
                cut_microtubule+=1;
                deb3("... ou là"); 
                k++;
            }
        }
        
        //detachement
        list<Id> lp = getListMicrotubules();
        for (list<Id>::iterator it=lp.begin();it!=lp.end();it++)
        {   

            Microtubule *m=getMicrotubule(*it);
            double proba = (float)rand()/(float)RAND_MAX;
            if ((proba < m_proba_detachement))
                {
                    deb3("testDetach...");
                    if (verbose==3){cout<<*m<<endl;}
                    //voir si je rajoute une condition sur l'etat du microtubule
                    m->setPropriete("gr_st_moins", -1);
                    detachements+=1;
                }
        }
                
        //initiation
//         double pos = (float)rand()/(float)RAND_MAX;
        double proba = (float)rand()/(float)RAND_MAX;
        double proba_initiation = m_params->getProprieteD("proba_initialisation_par_step");
        int p_proba_entier = proba_initiation;
        double p_proba_decimal = proba_initiation - p_proba_entier;
        //cout << "test de la valeur entière/décimale "<< p_proba_entier <<" "<<p_proba_decimal<<endl;
        int nb_init=0;
        for (int step=1;step<=p_proba_entier;step++)
        {
            deb3("testInit...");
            //vector<double> v = pointOnSphere(150, 3000,3000,3000);
            vector<double> v = pointOnContour(ep->getContour());
            //cout << v[0]<<"/" <<v[1]<<"/" <<v[2]<<"/" <<v[3]<<"/" <<v[4]<<"/" <<v[5] << endl;
            Id i = this->giveMicrotubule(ep, v);
            Microtubule *m = this->getMicrotubule(i);
            creation_microtubule+=1;
            if (verbose==3){cout<<*m<<endl;}
            nb_init++;
        }
        if (proba < p_proba_decimal)
        {   
            deb3("testInit decimal...");
            //vector<double> v = pointOnSphere(150, 3000,3000,3000);
            vector<double> v = pointOnContour(ep->getContour());
            //cout << v[0]<<"/" <<v[1]<<"/" <<v[2]<<"/" <<v[3]<<"/" <<v[4]<<"/" <<v[5] << endl;
            Id i = this->giveMicrotubule(ep, v);
            Microtubule *m = this->getMicrotubule(i);
            creation_microtubule+=1;
            if (verbose==3){cout<<*m<<endl;}
            nb_init++;
        }
        //cout<<"nb d'initiations "<<nb_init<<endl;
        
        deb2("           evolve microtubulepool 2");
//         int a=0;int b=0;int c=0;int d=0;double tot=0;int lg=0;
        lp = getListMicrotubules();
        deb2("           evolve microtubulepool 2b");
        for (list<Id>::iterator it=lp.begin();it!=lp.end();it++)
        {   
                //deb3("testMicr...");
                Microtubule *m=getMicrotubule(*it);
                deb2("                   evolve microtubulepool 3a");
                if (verbose>=1){cout << "                       in evolve microtubule, microtubule : " << *m <<endl;}
                m->evolve();
                if (verbose>=1){cout << "                       evolve returned in the pool loop : " << *m <<endl;}
                deb2("                   evolve microtubulepool 3b");
  
        }
        deb("           evolve microtubulepool 4");

    }



