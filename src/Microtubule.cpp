#include <deque>
#include <cstdlib>
#include "Microtubule.hpp"
#include "Structure.hpp"
#include "ElementPool.hpp"
#include "Parametres.hpp"
#include "MicrotubulePool.hpp"
#include "math.h"
#include "utility.hpp"

#define verbose 0
#define deb(x) if (verbose>=1){cout << x << endl;}
#define deb2(x) if (verbose>=2){cout << x << endl;}
#define deb3(x) if (verbose>=3){cout << x << endl;}

using namespace std;

std::ostream& Microtubule::Print( std::ostream &os) const
{
    os << "Microtubule "<<this->getId()<<"/lg= "<< this->length()<<" age "<<this->getAge()<<endl;    
return os;
}

ostream& operator<<(ostream& os, Structure& m)
{
    m.Print(os);
    return os;
}

ostream& operator<<(ostream& os, Microtubule& m)
{
    m.Print(os);
    return os;
}



Microtubule::Microtubule():Structure()
{
    deb("Microtubule()");
    m_id=0;
    m_type=1;
    m_age=0;
    this->std_properties();

    
}

Microtubule::Microtubule(Parametres *params, ElementPool *ep, MicrotubulePool *mp, Id r):Structure()
{
    deb("Microtubule(ElementPool *ep, MicrotubulePool *mp, Id r)");
    m_id=r;
    m_type=1;
    m_age=0;
    m_elementPool=ep;
    m_microtubulePool=mp;
    m_params=params;
    this->std_properties();
    Element *nouv = ep->giveElement();
    Anchor a;
    Shape s;
    vector<double> vec;

	double x = 500+(float)rand()/(float)RAND_MAX * 200;
	double y = 500+(float)rand()/(float)RAND_MAX * 200;
	double z = 500+(float)rand()/(float)RAND_MAX * 200;	
	vec.push_back(x);
	vec.push_back(y);
	vec.push_back(z);
	a.setPosition(vec);
	x = -1+2*(float)rand()/(float)RAND_MAX;
	y = -1+2*(float)rand()/(float)RAND_MAX;
	z = -1+2*(float)rand()/(float)RAND_MAX;
	double pond_alea=sqrt(pow(x,2) + pow(y,2) + pow(z,2));
	vector<double> dir;
	dir.push_back(x/pond_alea);
	dir.push_back(y/pond_alea);
	dir.push_back(z/pond_alea);
	s.setDirection(dir);

    nouv->setPrevious(NULL);
    nouv->setShape(s);
    nouv->setAnchor(a);
    nouv->setStructure(this);
    m_elementPool->spaceMtRegister(nouv);
	m_body.push_back( nouv );
}

Microtubule::Microtubule(Parametres *params, ElementPool *ep, MicrotubulePool *mp, Id r, vector<double> v):Structure()
{
    deb("Microtubule(ElementPool *ep, MicrotubulePool *mp, Id r, vector<double> v)");
    m_id=r;
    m_type=1;
    m_age=0;
    m_elementPool=ep;
    m_microtubulePool=mp;
    m_params=params;
    this->std_properties();
    deb("Microtubule() before Element *nouv");
    Element *nouv = ep->giveElement();
    Anchor a;
    Shape s;
// 	double x;
// 	double y;
// 	double z;
    vector<double> vec;
    vec.push_back(v[0]);
    vec.push_back(v[1]);
    vec.push_back(v[2]);
    vector<double> dir;
    dir.push_back(v[3]);
    dir.push_back(v[4]);
    dir.push_back(v[5]);
	a.setPosition(vec);
	s.setDirection(dir);

    nouv->setPrevious(NULL);
    nouv->setShape(s);
    nouv->setAnchor(a);
    nouv->setStructure(this);
    deb("Microtubule() before spaceMtRegister");
    m_elementPool->spaceMtRegister(nouv);
	m_body.push_back( nouv );
}

//TODO : voir si ce constructeur est nécessaire, auquel cas adapter à la version avec pointeurs
Microtubule::Microtubule(Parametres *params, ElementPool *ep, MicrotubulePool *mp, Id r, deque<Element *> body):Structure()
{
    deb("Microtubule(ElementPool *ep, MicrotubulePool *mp, Id r, deque<Id> body)");
    m_id=r;
    m_type=1;
    m_age=0;
    m_elementPool=ep;
    m_microtubulePool=mp;
    m_params=params;
    this->std_properties();
    for (size_t i = 0; i<body.size();i++)
    {
        body[i]->setStructure(this);
        m_body.push_back(body[i]);
        
    }
}

Microtubule::~Microtubule()
{
    //if (verbose){cout << "utilisation du destructeur Microtubule : "<<m_type<<" "<<m_id<<endl;}
}

void Microtubule::std_properties()
{

    m_proprietesI["gr_st_plus"]=1; //state of growth of the plus side
    m_proprietesI["gr_st_moins"]=0; //state of growth of the moins side
    
    //manque les conditions de nucleation 
}

void Microtubule::evolve()
{
    int toto=1;
    m_age++;
    if (m_proprietesI["gr_st_plus"]==1) //state of growth of the plus side
    {
        grow_plus();
    }
    if (m_proprietesI["gr_st_plus"]==0) //state of growth of the plus side
    {
        //grow_plus();
    }
    if (m_proprietesI["gr_st_plus"]==-1)
    {
        toto = shrink_plus();
    }
    if (toto)
	{
		if ((m_proprietesI["gr_st_moins"]==-1) /*&& (fmod(m_age,2)==0)*/) //state of growth of the moins side
		{
			shrink_moins(); 
		}
	}
    deb("fin evolve microtubule");
}

void Microtubule::evolve(int i)
{
    if (i == 1) {grow_plus();}
    if (i == 2) {shrink_plus();}
    if (i == 3) {shrink_moins();}
    if (i == 4) 
	{
// 		int s=m_body.size();
// 		double pos=(float)rand()/(float)RAND_MAX*s;
        //TODO ici souci de type
		//scission((int)pos);
	}
    m_age++;
}


void Microtubule::tag_tocut(Element *e)
{
    //this function will test the angle between the new tubulin and the previous one, and if necessary will enlist the tubulin in the tocut list
    if (m_params->getProprieteI("decision_cut"))
    {
        if ( e->getPrevious() )
        {
            vector<double> s1=e->getShape().getDirection();
            vector<double> s2=e->getPrevious()->getShape().getDirection();
            double a = angleVec(s1, s2);
            if ( (a >= m_params->getProprieteD("Angle_cut") ) && (e->getPrevious()->getPropriete("to_cut") != 1) && (e->getPropriete("to_cut") != 1))
            {
                //e->getPrevious()->setPropriete("to_cut", 1);
                m_elementPool->enlist_tocut(e->getPrevious());
                //cout << "en liste :"<<*(e) <<  endl;
                //cout << "\t"<<*(e->getPrevious()) <<  endl;
                //cout << "\t\t"<<*(e->getStructure()) <<  endl;
            }
        }
        else
        {
            printf("case of to_cut enlisting whereas no previous element exists");
            exit(0);
        }
    }
    
    
}

void Microtubule::grow_plus()
{
    //deb("grow plus");
// 	int cas = 0;
	//c'est ici que se concentre :
	//- le test de proximité des voisins, 
	//- le test de proximité du bord
	//récupération de l'id du plus actuel du microtubule
    //deb("grow plus 2");
	Element *previous=m_body.back();
    //cout<<"previous "<<*previous<<endl;

    //deb("grow plus 3");
    // test if the microtubule is empty (would not be normal)
	if (m_body.empty()){cout<<"VIDE!!!!"<<endl;exit(EXIT_FAILURE);}
	//on récupère les paramètres de l'ancien plus
    std::pair<double, Element*> prochesMt = m_elementPool->NN_Mt(previous);
    std::pair<double, Element*> prochesContour = m_elementPool->NN_Contour(previous);
    //cout << prochesMt.first << "  " << prochesContour.first<< endl;
    //TODO : revoir ces conditions:
    // cond 1 : distance au plus proche voisin inférieure à distance des paramètres
    // cond 2 : il y a un voisin (!=-1)
    // cond3 : le voisin prédit n'est pas la tubuline précédente
    // cond4 : la tubuline voisine n'a pas un age de 0
    //deb("grow plus 1");
    if ( (prochesMt.first < (m_params->getProprieteD("D_bundle") / m_params->getProprieteD("taille_microtubule"))) && (prochesMt.first != -1)  && ( prochesMt.second != previous ) && ( prochesMt.second->getAge() != 0 ) )
    {
        //if ( (prochesContour.first != 10000) && (prochesContour.first < 50) && (prochesContour.first != -1))
        //{
            //grow_plus_contact_mb(previous, prochesContour);
        //}
        //else
        {

            deb("grow plus | grow_plus_contact_voisin(previous, prochesMt)");
            grow_plus_contact_voisin(previous, prochesMt);
        }
        previous->setAge(1);
        //m_proprietesI["gr_st_plus"]=1;
    }
    else //je suis peut être libre ou proche d'une membrane
    {
        //cond1 : la distance n'est pas 10000 : useless condition cause 10000 is not assigned, it is 100000 -> sqrt
        //TODO  ---> l'enlever 
        //cond2 : la distance est inférieure à 100
        //cond3 : il y a un voisin
        if ( (prochesContour.first != 10000) && (prochesContour.first < 100) && (prochesContour.first != -1))
        {
            deb("grow plus | grow_plus_contact_mb(previous, prochesContour");
            grow_plus_contact_mb(previous, prochesContour);
        }
        else
        {
			//WARNING TODO : we never reach this condition, growth free is then in fact managed inside contact_mb code
			deb("grow plus | grow_plus_free(previous");
            grow_plus_free(previous);            
        }
        previous->setAge(1);
        //m_proprietesI["gr_st_plus"]=1;
    }


}

void Microtubule::grow_plus_contact_voisin(Element* previous, std::pair<double, Element *> proche)
{
    deb2("grow_plus_contact_voisin");
    deb3("grow_plus_contact_voisin");
    vector<double> dir;
    double a;

    Element *el = proche.second;
    el->setPropriete("contact", 1);
    //cout << m_elementPool->getElement(proche.second, "grow plus 0")->getPropriete("contact") << endl;		
    //ici je peux d'ores et déjà prévoir la condition "passe ou aligne toi" : il s'agit de comparer s et el->getShape()
    vector<double> s1=previous->getShape().getDirection();
    Anchor *anc = previous->getAnchor();
    double aX=*anc->getX();
    double aY=*anc->getY();
    double aZ=*anc->getZ();
    vector<double> s2=el->getShape().getDirection();
    a = angleVec(s1, s2);
    //cout<<"angle "<<a;
    deb2("grow_plus_contact_voisin 1");
    if ( (a < m_params->getProprieteD("Angle_bundle")) | (a > (3.141592653589793 - m_params->getProprieteD("Angle_bundle"))) )
    {
        if (previous->getPropriete("contact") != 1)
        {
            m_elementPool->rencontre_bundle_grow+=1;
        }
        
        //cout<<"\t glisse ";
        if (a < m_params->getProprieteD("Angle_bundle"))
        {
            //cout<<" dans le meme sens "<<endl;
            dir.push_back(s2[0]);
            dir.push_back(s2[1]);
            dir.push_back(s2[2]);
        }
        if (a > (3.141592653589793 - m_params->getProprieteD("Angle_bundle")))
        {
            //cout<<" dans l'autre sens "<<endl;
            dir.push_back(-s2[0]);
            dir.push_back(-s2[1]);
            dir.push_back(-s2[2]);
        }
        
        Element  *nouv=m_elementPool->giveElement();
        nouv->setStructure(this);
        //placement de son id dans le microtubule
        m_body.push_back(nouv);
        deb2("grow_plus_contact_voisin 2");
        //positionnement à la suite de l'élément précédent
        vector<double> pos;
        pos.push_back( aX + s1[0]);
        pos.push_back( aY + s1[1]);
        pos.push_back( aZ + s1[2] );
        Anchor a2(pos[0], pos[1],pos[2]);
        deb2("grow_plus_contact_voisin 3");
        nouv->setAnchor(a2);    
        //on créé la nouvelle direction et on l'assigne
        Shape s2(dir[0], dir[1],dir[2]);
        nouv->setShape(s2);    
        nouv->setPrevious(previous);
        nouv->setPropriete("contact", 1.);
        nouv->setPropriete("nearest_contour",el->getPropriete("nearest_contour"));
		nouv->setPropriete("nearest_contour_vtk",el->getPropriete("nearest_contour_vtk"));
		nouv->setPropriete("nearest_contour_d",el->getPropriete("nearest_contour_d"));
		nouv->setPropriete("D_pos_dir2lim_mbFn",el->getPropriete("D_pos_dir2lim_mbFn"));
		nouv->setPropriete("cortical",el->getPropriete("cortical"));
        m_elementPool->spaceMtRegister(nouv);
        tag_tocut(nouv);

    }
    else//si l'angle est mauvais ->shrink
    {
        if (m_proprietesI["gr_st_plus"] != m_params->getProprieteI("decision_rencontre"))
        {
            m_elementPool->rencontre_bundle_shrink+=1;
        }
        m_proprietesI["gr_st_plus"]=m_params->getProprieteI("decision_rencontre");
        //cout<<"\t stoppe "<<endl;
    }
    
    
    deb3("FIN grow_plus_contact_voisin");
    
}

void Microtubule::grow_plus_contact_mb(Element *plus, std::pair<double, Element*> proche_paroi)
{
    //TODO (13 05 13) : vérifier peut être la normalité de tous les vecteurs du contour?
    deb2("grow_plus_contact_mb");
    //deb2("grow_plus_contact_mb");        
    //initialisation des vecteurs, directions et positions
    vector<double> pos_dir;
    vector<double> pos_dir2pos_mb;
    vector<double> n_perp;
    vector<double> dir_cand;
    
    int tag_cortical=0;//le tag utilisé pour définir si le nouvel élément sera proche de la membrane
    
    Shape Sdir_prev = plus->getShape();
    vector<double> dir_prev;
    dir_prev.push_back(Sdir_prev.getDirection()[0]);
    dir_prev.push_back(Sdir_prev.getDirection()[1]);
    dir_prev.push_back(Sdir_prev.getDirection()[2]);    
    Anchor *pos_prev = plus->getAnchor();
    
    Element *paroi = proche_paroi.second;
    paroi->setPropriete("contact", 1.);
    Shape n = paroi->getShape();
    Anchor *pos_mb = paroi->getAnchor();
    
    vector<double> influence = paroi->getPropriete_v("strain_0");
    double attache = paroi->getPropriete("attachement");
    //cout << "attache :"<< attache.size() <<endl;
    //ok un peu étrange, mais je transforme "attache" en "accrochage"... 
    int accrochage=0;
    if ((attache == 1) )
    {
		accrochage=1;
	}
	//cout << "accrochage :"<< accrochage <<endl;
    //TODO : voir si ce calcul vaut la peine
    //calcul des coordonnees du vecteur dir_prev_perp, situé dans le plan de dir_prev,n
    deb2("calcul des coordonnees du vecteur dir_prev_perp, situé dans le plan de dir_prev/n et perpendiculaire à dir prev");
    vector<double> dir_prev_perp;
    double scalaire = dir_prev[0] * n.getDirection()[0] + dir_prev[1] * n.getDirection()[1] + dir_prev[2] * n.getDirection()[2];
    dir_prev_perp.push_back(n.getDirection()[0] - scalaire * dir_prev[0]);
    dir_prev_perp.push_back(n.getDirection()[1] - scalaire * dir_prev[1]);
    dir_prev_perp.push_back(n.getDirection()[2] - scalaire * dir_prev[2]);
    dir_prev_perp=normalise(dir_prev_perp);
    
    deb2("calcul des coordonnees du vecteur n_perp, situé dans le plan de dir_prev/n et perpendiculaire à n");
    n_perp.push_back(dir_prev[0] - scalaire * n.getDirection()[0]);
    n_perp.push_back(dir_prev[1] - scalaire * n.getDirection()[1]);
    n_perp.push_back(dir_prev[2] - scalaire * n.getDirection()[2]);
    n_perp=normalise(n_perp);
    
    deb2("calcul des coordonnées du point pos_dir : point prévisionnel de la prochaine tubuline");
    pos_dir.push_back(*pos_prev->getX() + dir_prev[0]);
    pos_dir.push_back(*pos_prev->getY() + dir_prev[1]);
    pos_dir.push_back(*pos_prev->getZ() + dir_prev[2]);
    
    deb2("calcul des coordonnées du vecteur pos_dir2pos_mb");
    pos_dir2pos_mb.push_back(*pos_mb->getX() - pos_dir[0]);
    pos_dir2pos_mb.push_back(*pos_mb->getY() - pos_dir[1] );
    pos_dir2pos_mb.push_back(*pos_mb->getZ() - pos_dir[2] );
    deb2("calcul de la distance D_pos_dir_alea2pos_mbFn");
    double D_pos_dir2pos_mbFn = pos_dir2pos_mb[0] * n.getDirection()[0] + pos_dir2pos_mb[1] * n.getDirection()[1] + pos_dir2pos_mb[2] * n.getDirection()[2];
        //si c'est négatif il y a sans doute un souci, même si une courbure importante peut sans doute l'expliquer
        //TODO(13 05 13) ajouter la verif
        //deb2(D_pos_dir2pos_mbFn);
        //A noter : ici d_mb est donc au carré puisque la distance n'est pas en sqrt
    double D_pos_dir2lim_mbFn = D_pos_dir2pos_mbFn - m_params->getProprieteD("d_mb");
    //if ( (D_pos_dir2lim_mbFn < 0) | (m_params->getProprieteI("decision_accrochage")))
    //je rajoute la condition accrochage, qui vaut 0 par défaut si je n'ai pas de donnée d'accrochage et 1 si j'ai une donnée et qu'elle me dit de m'accrocher.
    //cond1 : la distance entre le point prévu et la membrane est inférieure à 0 == le point prévu est hors de la structure
    //OU
    //cond2 : le paramètre decision_accrochage vaut 1
    //OU
    //cond3 : le point du mesh le plus proche a 1 en "attachement" ET le point précédent est cortical
    if ( (D_pos_dir2lim_mbFn < 0) | (m_params->getProprieteI("decision_accrochage")  ) | ( (accrochage==1) && (plus->getPropriete("cortical")==1) )  )
    {
		//cout << "accrochage :"<< accrochage <<endl;
		tag_cortical=1;
        deb2("on est repoussé");
        deb2("calcul des deux vecteurs maximum possibles");
        vector<double> dir_cand_extr1;
        vector<double> dir_cand_extr2;
        dir_cand_extr1.push_back(dir_prev_perp[0]*sin(m_params->getProprieteD("Angle_mb_trajectoire")) + dir_prev[0]*cos(m_params->getProprieteD("Angle_mb_trajectoire")));
        dir_cand_extr1.push_back(dir_prev_perp[1]*sin(m_params->getProprieteD("Angle_mb_trajectoire")) + dir_prev[1]*cos(m_params->getProprieteD("Angle_mb_trajectoire")));
        dir_cand_extr1.push_back(dir_prev_perp[2]*sin(m_params->getProprieteD("Angle_mb_trajectoire")) + dir_prev[2]*cos(m_params->getProprieteD("Angle_mb_trajectoire")));
        dir_cand_extr2.push_back(- dir_prev_perp[0]*sin(m_params->getProprieteD("Angle_mb_trajectoire")) + dir_prev[0]*cos(m_params->getProprieteD("Angle_mb_trajectoire")));
        dir_cand_extr2.push_back(- dir_prev_perp[1]*sin(m_params->getProprieteD("Angle_mb_trajectoire")) + dir_prev[1]*cos(m_params->getProprieteD("Angle_mb_trajectoire")));
        dir_cand_extr2.push_back(- dir_prev_perp[2]*sin(m_params->getProprieteD("Angle_mb_trajectoire")) + dir_prev[2]*cos(m_params->getProprieteD("Angle_mb_trajectoire")));
        deb2("calcul des deux vecteurs maximum possibles 2");

        //calcul des deux scalaires theoriques
        //TODO : ici souci ce n'est pas la taille du vecteur qui compte mais vecteur + point d'origine
        double dir_cand_extr1_proj = dir_cand_extr1[0] * n.getDirection()[0] + dir_cand_extr1[1] * n.getDirection()[1] + dir_cand_extr1[2] * n.getDirection()[2];
        double dir_cand_extr2_proj = dir_cand_extr2[0] * n.getDirection()[0] + dir_cand_extr2[1] * n.getDirection()[1] + dir_cand_extr2[2] * n.getDirection()[2];
        //deb(dir_cand_extr2[0]);
        //conds : l'idée était de voir si le scalaire de la nouvelle direction fait passer au travers la membrane Ces conditions ne peuvent être réalisées que si on a une D_pos_dir2lim_mbFn < 1 ou > -1 
        if (
        ( (dir_cand_extr1_proj > D_pos_dir2lim_mbFn) && (dir_cand_extr2_proj < D_pos_dir2lim_mbFn ) )
        |
        ( (dir_cand_extr1_proj < D_pos_dir2lim_mbFn) && (dir_cand_extr2_proj > D_pos_dir2lim_mbFn ) )
        )
        {
            deb2("on est dans le cas d'une intersection");
            //cette valeur "dangereuse" ne cause pas d'erreur car les conditions font que 0<||D_pos_dir2lim_mbFn||<1
            double perp=sqrt(1-D_pos_dir2lim_mbFn*D_pos_dir2lim_mbFn);
            //ici on rapproche du point d'équilibre
            dir_cand.push_back(D_pos_dir2lim_mbFn*n.getDirection()[0] + perp*n_perp[0]);
            dir_cand.push_back(D_pos_dir2lim_mbFn*n.getDirection()[1] + perp*n_perp[1]);
            dir_cand.push_back(D_pos_dir2lim_mbFn*n.getDirection()[2] + perp*n_perp[2]);
            
        }
        else
        {
            deb2("on n'est pas dans le cas d'une intersection");
            double un = fabs(dir_cand_extr1_proj - D_pos_dir2lim_mbFn);
            double deux = fabs(dir_cand_extr2_proj - D_pos_dir2lim_mbFn);
            if (un < deux)
            {
                dir_cand.push_back(dir_cand_extr1[0]);
                dir_cand.push_back(dir_cand_extr1[1]);
                dir_cand.push_back(dir_cand_extr1[2]);
            }
            else
            {
                dir_cand.push_back(dir_cand_extr2[0]);
                dir_cand.push_back(dir_cand_extr2[1]);
                dir_cand.push_back(dir_cand_extr2[2]);
            }
        }
        dir_cand=normalise(dir_cand);
    }
    //cond1 : la distance à la membrane du point prévu est inférieure à 'épaisseur corticale
    //ET
    //cond2 : le point est sous la membrane
    //ET
    //cond3 : la direction du nouveau vecteur est vers l'extérieur
    else if ( (D_pos_dir2lim_mbFn < m_params->getProprieteD("epaisseur_corticale")) && (D_pos_dir2lim_mbFn > 0) && (scalaire > 0) )
    {
		//ici je donne la propriété d'être proche de la zone corticale, condition sine qua none pour s'accrocher
		tag_cortical=1;
        //la nouvelle direction est perpendiculaire à la membrane
        dir_cand.push_back(n_perp[0]);
        dir_cand.push_back(n_perp[1]);
        dir_cand.push_back(n_perp[2]);
        dir_cand=normalise(dir_cand);
    }
    else
    {
        deb2("on est cytosolique");
        tag_cortical=0;
        dir_cand.push_back(dir_prev[0]);
        dir_cand.push_back(dir_prev[1]);
        dir_cand.push_back(dir_prev[2]);
        dir_cand=normalise(dir_cand);
        
        
    }

    deb2("ajout du facteur aleatoire");
    double x = -1.+2*(float)rand()/(float)RAND_MAX;
    double y = -1.+2*(float)rand()/(float)RAND_MAX;
    double z = -1.+2*(float)rand()/(float)RAND_MAX;    
    int fixe=m_params->getProprieteD("part_alea_fixe");
	int alea=m_params->getProprieteD("part_alea_alea");
    dir_cand[0]=fixe*dir_cand[0]  + alea*x;
    dir_cand[1]=fixe*dir_cand[1]  + alea*y;
    dir_cand[2]=fixe*dir_cand[2]  + alea*z;
    dir_cand=normalise(dir_cand);//pour le moment on ne pondere pas pas la future definition de l'angle max
    
    //ici le cas où on a des champs de vecteur influençant la trajectoire
    if ((influence.size() == 3) && (m_microtubulePool->getInfluence()>0) )
    {
        deb2("utilisation de l'influence");
        //influence du comportement standart
        double f1 = m_microtubulePool->getInfluence();
        //pondération de l'influence des directions venant de la donnée
        double f2 = m_params->getProprieteD("part_influence_influence");
        //cout << "utilisation de l'influence" <<endl;
        double sc_infl = influence[0]*n.getDirection()[0] + influence[1]*n.getDirection()[1] + influence[2]*n.getDirection()[2];
        vector<double > influence_v;
        influence_v.push_back(influence[0] - sc_infl * n.getDirection()[0]);
        influence_v.push_back(influence[1] - sc_infl * n.getDirection()[1]);
        influence_v.push_back(influence[2] - sc_infl * n.getDirection()[2]);
        double sens = influence_v[0]*dir_cand[0] + influence_v[1]*dir_cand[1] + influence_v[2]*dir_cand[2];
        //cout << "dir_cand 0 : "<<dir_cand[0]<< " dir_cand 1 : "<<dir_cand[1]<< " dir_cand 2 : "<<dir_cand[2] <<  endl;
        influence_v=normalise(influence_v);
        //cout << "influence 0 : "<<influence_v[0]<< " influence 1 : "<<influence_v[1]<< " influence 2 : "<<influence_v[2] <<  endl;
        if (sens >0)
        {
            dir_cand[0] = f1*dir_cand[0] + f2*influence_v[0];
            dir_cand[1] = f1*dir_cand[1] + f2*influence_v[1];
            dir_cand[2] = f1*dir_cand[2] + f2*influence_v[2];
            dir_cand=normalise(dir_cand);
        }
        else
        {
            dir_cand[0] = f1*dir_cand[0] - f2*influence_v[0];
            dir_cand[1] = f1*dir_cand[1] - f2*influence_v[1];
            dir_cand[2] = f1*dir_cand[2] - f2*influence_v[2];
            dir_cand=normalise(dir_cand);
        }
    }

    //ici je vais mettre la condition liée à l'angle max. Si l'angle prédit est trop grand, je ne rajoute pas d'élément.
    if ( ( angleVec(dir_cand, dir_prev) > m_params->getProprieteD("Angle_mb_limite") ) )
    {
        //cout << "angle trop aigu" << angleVec(dir_cand, dir_prev) << endl;
        m_params->setProprieteI("stop",0);
        if (m_proprietesI["gr_st_plus"] != m_params->getProprieteI("decision_rencontre"))
        {
            m_elementPool->rencontre_mb_shrink+=1;
        }
        m_proprietesI["gr_st_plus"]=m_params->getProprieteI("decision_rencontre");
    }

    deb2("creation de l'element");
    Element *nouv=m_elementPool->giveElement();
    nouv->setStructure(this);
    //placement de son id dans le microtubule
    m_body.push_back(nouv);
    //positionnement à la suite de l'élément précédent
    deb2("creation de l'element Anchor");
    vector<double> pos;
    pos.push_back( *pos_prev->getX() + dir_prev[0]);
    pos.push_back( *pos_prev->getY() + dir_prev[1]);
    pos.push_back( *pos_prev->getZ() + dir_prev[2] );
    Anchor a2(pos[0], pos[1],pos[2]);
    nouv->setAnchor(a2);    
    nouv->setPrevious(plus);

    deb2("creation de l'element Shape");
    //on créé la nouvelle direction et on l'assigne
    Shape s2(dir_cand[0], dir_cand[1],dir_cand[2]);            
    nouv->setShape(s2);
    //cout << c<< endl;
    //cout << *nouv << endl;
    m_elementPool->spaceMtRegister(nouv);
    nouv->setPropriete("nearest_contour",paroi->getId());
    nouv->setPropriete("nearest_contour_vtk",paroi->getId_vtk());
    nouv->setPropriete("nearest_contour_d",proche_paroi.first);
    nouv->setPropriete("D_pos_dir2lim_mbFn",D_pos_dir2lim_mbFn);
    //les conditions pour être en accrochage
    if (plus->getPropriete("cortical")==0)
    {
        m_elementPool->rencontre_mb_grow+=1;
    }
    if (tag_cortical==1){nouv->setPropriete("cortical",1);}
    else {nouv->setPropriete("cortical",0);}
    
    if ( (plus->getPropriete("cortical")==1) && nouv->getPropriete("cortical") == 0)
    {
        m_elementPool->quitte_mb+=1;
    }
    
    tag_tocut(nouv);
    //cout << nouv << *nouv << endl;
    deb2("FIN grow_plus_contact_mb");

}

void Microtubule::grow_plus_free(Element *plus)
{
    deb("grow_plus_free");
    deb3("grow_plus_free");
    vector<double> dir;
    Anchor *a = plus->getAnchor();
    Shape s = plus->getShape();
    //ici : trajectoire sans influence, donc je rajoute juste un peu de stochasticité
    //création d'un vecteur aléatoire
    double x = -1.+2*(float)rand()/(float)RAND_MAX;
    double y = -1.+2*(float)rand()/(float)RAND_MAX;
    double z = -1.+2*(float)rand()/(float)RAND_MAX;
    int fixe=m_params->getProprieteD("part_alea_fixe");
	int alea=m_params->getProprieteD("part_alea_alea");
    x=fixe*s.getDirection()[0]  + alea*x;
    y=fixe*s.getDirection()[1]  + alea*y;
    z=fixe*s.getDirection()[2]  + alea*z;
    //normalisation
    double pond_dir=sqrt(pow(x,2) + pow(y,2) + pow(z,2) );
    dir.push_back( x/pond_dir );
    dir.push_back( y/pond_dir );
    dir.push_back( z/pond_dir );
    //cout << dir[0] << " face a ";
    Element *nouv=m_elementPool->giveElement();
    //TODO ici ajouter le fait de donner son previous à l'élément
    nouv->setPrevious(plus);
    
    nouv->setStructure(this);
    //placement de son id dans le microtubule
    m_body.push_back(nouv);
    //positionnement à la suite de l'élément précédent
    vector<double> pos;
    pos.push_back( *a->getX() + s.getDirection()[0]);
    pos.push_back( *a->getY() + s.getDirection()[1]);
    pos.push_back( *a->getZ() + s.getDirection()[2] );
    Anchor a2(pos[0], pos[1],pos[2]);
    nouv->setAnchor(a2);    
    //ici ce qui gère la nouvelle direction

    //on créé la nouvelle direction et on l'assigne
    Shape s2(dir[0], dir[1],dir[2]);
    nouv->setShape(s2);   
    m_elementPool->spaceMtRegister(nouv);    
    
    if (plus->getPropriete("cortical")==1)
    {
        m_elementPool->quitte_mb+=1;
    }
}


int Microtubule::shrink_plus()
{
//here the function is not void because of a hack... I do not know why but suddenly erasing the object from the unordered_map behaved as a destruction of the object. So a shrink_plus could not be followed by a shrink_moins if ever the former destroyed the microtubule. Here the shrink plus informs the evolve() function that it is not possible to go to shrink_moins
    deb("shrink plus"); 
    if ( !m_body.empty() )
    {
        deb("shrink plus !body empty"); 
		  Element *i = m_body.back();
          deb2("shrink plus !body empty setStructure(NULL)"); 
		  i->setStructure(NULL);
          deb2("shrink plus !body empty erase(m_body.begin()"); 
		  m_body.erase(m_body.end());
		  m_elementPool->erase(i);
	}
	if ( m_body.empty() )
	{
        deb("shrink plus body empty");
        //cout <<"test 1 : "<< m_proprietesI["gr_st_moins"]<<endl;
		m_microtubulePool->erase(m_id);
		deb("shrink plus OVER"); 
		return 0;
	}
	deb("shrink plus OVER"); 
	return 1;
}

//TODO : traduire ça en pointeurs, ce n'est pas trop trivial...
void Microtubule::shrink_moins()
{
    deb("shrink moins"); 
    if ( !m_body.empty() )
    {
        deb("shrink moins !body empty"); 
        Element *i = m_body.front();
        //cout << "shrink moins" << *i << endl;
        deb("shrink moins !body empty setStructure(NULL)"); 
        i->setStructure(NULL);
        deb("shrink moins !body empty erase(m_body.begin()"); 
        m_body.erase(m_body.begin());
        Element *next = m_body.front();
        next->setPrevious(NULL);
        deb("shrink moins !body empty erase(i)"); 
        m_elementPool->erase(i); 
    }
    if ( m_body.empty() )
	{
        deb("shrink moins body empty");
		m_microtubulePool->erase(m_id);
	}
}

//TODO : commence à être pas toute jeune, à vérifier d'autant que je viens de faire la traduction en pointeurs
void Microtubule::scission(Element *element)
{
    deb("scission"); 
    
    
    deb3("scission"); 
	int s = m_body.size(); 
    //cout << "taille "<<s<<" " << element->getPrevious()<<endl;
    //if (element == m_body.front()) cout << "premier element"<<endl;
    deb3("scission 2"); 
	if ( (s >=3 ) && ( element != m_body.front() )  )
	{
// 		int position;
		deque<Element *> first;
		deque<Element *> second;
		int b=0;
		int k =0;
        int rang=0;
		for (deque<Element *>::iterator it = m_body.begin(); it < m_body.end(); it++)
		{
			if (*it == element) {b=1;k=rang;(*it)->setPrevious(NULL);}
			if (b==0){first.push_back(*it);}
			if (b==1){second.push_back(*it);}
            //if (b==1){b=2;}            
            rang++;
		}
        if (verbose==2){cout << "scission pos "<< k <<endl;}
        deb3("scission 3"); 
		m_body=first;
        //if (m_body.size() == 0) cout << "1 vide des la scission"<<endl;
        //if (m_body.size() == 1) cout << "1 un seul element à la scission"<<endl;
        //if (second.size() == 0) cout << "2 vide des la scission"<<endl;
        //if (second.size() == 1) cout << "2 un seul element à la scission"<<endl;
        m_proprietesI["gr_st_plus"]=-1;
        deb3("scission 4"); 
        if (second.size() != 0)
        {
            Id i=m_microtubulePool->giveMicrotubule(m_elementPool, second);
            for (deque<Element *>::iterator it = second.begin(); it < second.end(); it++)
            {
                (*it)->setStructure(m_microtubulePool->getMicrotubule(i));
            }
            m_microtubulePool->getMicrotubule(i)->setAge(m_age);
            m_microtubulePool->getMicrotubule(i)->setPropriete("gr_st_moins", -1);
        }
        deb3("scission 5"); 
	}
}

double distance(vector<double> pos_point, vector<double> pos_ref, vector<double> vecteur_ref)
{
    vector<double> pos_point2pos_ref;
    pos_point2pos_ref.push_back(pos_ref[0] - pos_point[0]);
    pos_point2pos_ref.push_back(pos_ref[1] - pos_point[1]);
    pos_point2pos_ref.push_back(pos_ref[2] - pos_point[2]);
    double D_pos_point2pos_refFn = pos_point2pos_ref[0] * vecteur_ref[0] + pos_point2pos_ref[1] * vecteur_ref[1] + pos_point2pos_ref[2] * vecteur_ref[2];
    return D_pos_point2pos_refFn;
}


std::deque<Element *> Microtubule::getBody() const
{
    return m_body;    
}

void Microtubule::setBody(std::deque<Element *> body) 
{
    for (size_t i = 0; i<body.size();i++)
    {
        m_body.push_back(body[i]);
        body[i]->setStructure(this);
    }
}

int Microtubule::length() const
{
    if ( m_body.empty() ) { return -1; }
    else { return m_body.size();}
	
}

Element *Microtubule::getPlus()
{
    return m_body.back();
}



Element * Microtubule::getPosition2ElementId(int position)
{
    return m_body[position];
}

ElementPool* Microtubule::getElementPool()
{
    return m_elementPool;
}


void Microtubule::setPropriete(string p, double k)					{m_proprietesD[p]=k;}
void Microtubule::setPropriete(string p, int k)     					{m_proprietesI[p]=k;}
double Microtubule::getProprieteD(string p)							{return m_proprietesD[p];}
double Microtubule::getProprieteI(string p)							{return m_proprietesI[p];}
void Microtubule::setAge(int age)                                     {m_age=age;}
int Microtubule::getAge() const                                       {return m_age;}




